package namics.skillgroup.themoviedb.data;

import java.util.ArrayList;

/**
 * Created by plopez on 21/04/17.
 */

public class Result {

    String poster_path;
    boolean adult;
    String overview;
    String release_date;
    String original_title;
    ArrayList<Integer> genre_ids;
    int id;
    String media_type;
    String original_language;
    String title;
    String backdrop_path;
    double popularity;
    int vote_count;
    boolean video;
    double vote_average;

    public String getPosterPath() {
        return poster_path;
    }

    public String getBackdropPath() {
        return backdrop_path;
    }

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }
}
