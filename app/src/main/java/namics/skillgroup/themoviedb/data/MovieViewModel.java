package namics.skillgroup.themoviedb.data;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import namics.skillgroup.themoviedb.BR;
import namics.skillgroup.themoviedb.TheMovieDbApplication;

import static android.content.ContentValues.TAG;

/**
 * Created by plopez on 21/04/17.
 */

public class MovieViewModel extends BaseObservable {

    @Inject
    EventBus eventBus;

    private Result result;

    public MovieViewModel() {
        final TheMovieDbApplication application = TheMovieDbApplication.app();
        if (application != null) {
            application.basicComponent().inject(this);
        }
    }

    @Bindable
    public void setResult(Result result) {
        this.result = result;
        notifyPropertyChanged(BR._all);
    }

    @NonNull
    public String title() {
        if (result != null) {
            return result.getTitle();
        } else {
            return "Empty Item";
        }
    }

    @NonNull
    public String overview() {
        if (result != null) {
            return result.getOverview();
        } else {
            return "No Overview";
        }
    }

    public View.OnClickListener onClickMovie() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "OnMovieClicked: " + title());
                eventBus.post(new OnMovieClickedEvent(result));
            }
        };
    }

    public String getPosterPath() {
        return result.getPosterPath();
    }

    public String getBackdropPath() {
        return result.getBackdropPath();
    }

    public static class OnMovieClickedEvent {

        private Result result;

        public OnMovieClickedEvent(Result result) {
            this.result = result;
        }

        public Result getResult() {
            return result;
        }
    }
}
