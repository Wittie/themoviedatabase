package namics.skillgroup.themoviedb.data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by plopez on 21/04/17.
 */

public interface EndPointInterface {

    String SERVER = "https://api.themoviedb.org/";
    String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w300";

    String ENDPOINT_LIST = "4/list/{page}";
    String API_KEY = "93aea0c77bc168d8bbce3918cefefa45";

    @GET(ENDPOINT_LIST)
    Call<DataModel> getMoviesList(@Path("page") int page, @Query("api_key") String apiKey);

}
