package namics.skillgroup.themoviedb.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by plopez on 21/04/17.
 */

public class DataModel {

    String poster_path;
    int id;
    String backdrop_path;
    int total_results;
    String revenue;
    int page;
    ArrayList<Result> results;
    int total_pages;
    String iso_639_1;
    String sort_by;
    String description;
    Object created_by;
    String iso_3166_1;
    double average_rating;
    int runtime;
    String name;
    Object comments;

    @SerializedName("public")
    boolean isPublic;

    public Result[] getResults() {
        return results.toArray(new Result[results.size()]);
    }

    public int getCurrentPage() {
        return id;
    }

    public int getTotalPages() {
        return total_pages;
    }

}
