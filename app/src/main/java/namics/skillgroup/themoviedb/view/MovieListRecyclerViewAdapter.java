package namics.skillgroup.themoviedb.view;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import namics.skillgroup.themoviedb.R;
import namics.skillgroup.themoviedb.data.Result;

/**
 * Created by plopez on 21/04/17.
 */

class MovieListRecyclerViewAdapter extends RecyclerView.Adapter<MovieListRecyclerViewHolder> {

    private Result[] results;

    @Override
    public MovieListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_list_result, parent, false);
        return new MovieListRecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(MovieListRecyclerViewHolder holder, int position) {
        holder.bind(results[position]);
    }

    @Override
    public int getItemCount() {
        return results != null ? results.length : 0;
    }

    public void setNewData(Result[] results) {
        this.results = results;
        notifyDataSetChanged();
    }
}
