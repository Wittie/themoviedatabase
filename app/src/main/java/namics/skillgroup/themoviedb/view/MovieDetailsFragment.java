package namics.skillgroup.themoviedb.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import namics.skillgroup.themoviedb.R;
import namics.skillgroup.themoviedb.data.EndPointInterface;
import namics.skillgroup.themoviedb.data.MovieViewModel;
import namics.skillgroup.themoviedb.data.Result;
import namics.skillgroup.themoviedb.databinding.FragmentMovieDetailsBinding;

import static android.content.ContentValues.TAG;

/**
 * Created by plopez on 21/04/17.
 */

public class MovieDetailsFragment extends Fragment implements RequestListener<String, GlideDrawable> {

    MovieViewModel movieViewModel = new MovieViewModel();
    private FragmentMovieDetailsBinding binding;

    public void setNewViewModel(@NonNull Result result) {
        movieViewModel.setResult(result);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details, container, false);
        binding.setMovieViewModel(movieViewModel);

        downloadImage(binding.posterImageView, movieViewModel.getPosterPath());

        return binding.getRoot();
    }

    public void downloadImage(ImageView imageView, String path) {
        Glide.with(binding.getRoot().getContext())
                .load(EndPointInterface.IMAGE_BASE_URL + path)
                .listener(this)
                .into(imageView);
    }

    @Override
    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
        Log.d(TAG, "onException: " + e.getLocalizedMessage());
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
        return false;
    }
}