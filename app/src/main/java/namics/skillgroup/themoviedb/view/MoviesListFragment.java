package namics.skillgroup.themoviedb.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import namics.skillgroup.themoviedb.R;
import namics.skillgroup.themoviedb.data.Result;
import namics.skillgroup.themoviedb.databinding.FragmentListMoviesBinding;

/**
 * Created by plopez on 21/04/17.
 */

public class MoviesListFragment extends Fragment {

    private MovieListRecyclerViewAdapter adapter = new MovieListRecyclerViewAdapter();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final FragmentListMoviesBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_movies, container, false);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recyclerview_divider));

        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(dividerItemDecoration);

        return binding.getRoot();
    }

    public void setNewData(Result[] results) {
        adapter.setNewData(results);
    }

}
