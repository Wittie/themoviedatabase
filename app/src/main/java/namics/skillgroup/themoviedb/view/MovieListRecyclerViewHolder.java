package namics.skillgroup.themoviedb.view;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import namics.skillgroup.themoviedb.BR;
import namics.skillgroup.themoviedb.data.MovieViewModel;
import namics.skillgroup.themoviedb.data.Result;

/**
 * Created by plopez on 21/04/17.
 */

class MovieListRecyclerViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding viewDataBinding;
    private final MovieViewModel movieViewModel;

    public MovieListRecyclerViewHolder(ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        viewDataBinding = itemBinding;
        movieViewModel = new MovieViewModel();
    }

    public void bind(Result result) {
        movieViewModel.setResult(result);
        viewDataBinding.setVariable(BR.viewModel, movieViewModel);
        viewDataBinding.executePendingBindings();
    }

}
