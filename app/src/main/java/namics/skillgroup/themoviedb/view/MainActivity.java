package namics.skillgroup.themoviedb.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import namics.skillgroup.themoviedb.R;
import namics.skillgroup.themoviedb.TheMovieDbApplication;
import namics.skillgroup.themoviedb.data.DataModel;
import namics.skillgroup.themoviedb.data.EndPointInterface;
import namics.skillgroup.themoviedb.data.MovieViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<DataModel> {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Inject
    EndPointInterface endPointInterface;

    @Inject
    EventBus eventBus;

    MoviesListFragment listFragment;
    MovieDetailsFragment detailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);
        TheMovieDbApplication.app().basicComponent().inject(this);

        setUpMoviesListFragment();
        setUpMovieDetailsFragment();

        startDownloadingMoviesList();

        showFragment(listFragment);
    }

    private void startDownloadingMoviesList() {
        Call<DataModel> call = endPointInterface.getMoviesList(1, EndPointInterface.API_KEY);
        call.enqueue(this);
    }

    private void setUpMoviesListFragment() {
        listFragment = (MoviesListFragment) getSupportFragmentManager().findFragmentByTag(MoviesListFragment.class.getSimpleName());
        if (listFragment == null) {
            listFragment = new MoviesListFragment();
        }
    }

    private void setUpMovieDetailsFragment() {
        detailsFragment = (MovieDetailsFragment) getSupportFragmentManager().findFragmentByTag(MovieDetailsFragment.class.getSimpleName());
        if (detailsFragment == null) {
            detailsFragment = new MovieDetailsFragment();
        }
    }

    protected void showFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    @Override
    public void onResponse(Call<DataModel> call, Response<DataModel> response) {
        Log.d(TAG, "onResponse: " + response.message());
        listFragment.setNewData(response.body().getResults());
    }

    @Override
    public void onFailure(Call<DataModel> call, Throwable t) {
        Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
    }

    @Subscribe
    public void onMessageEvent(MovieViewModel.OnMovieClickedEvent event) {
        detailsFragment.setNewViewModel(event.getResult());
        showFragment(detailsFragment);
    }

}
