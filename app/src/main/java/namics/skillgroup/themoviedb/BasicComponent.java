package namics.skillgroup.themoviedb;

import javax.inject.Singleton;

import dagger.Component;
import namics.skillgroup.themoviedb.data.MovieViewModel;
import namics.skillgroup.themoviedb.view.MainActivity;

/**
 * Created by pablo on 24/04/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface BasicComponent {

    void inject(MainActivity mainActivity);

    void inject(MovieViewModel movieViewModel);

}