package namics.skillgroup.themoviedb;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import namics.skillgroup.themoviedb.data.EndPointInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pablo on 24/04/17.
 */
@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }

    @Provides
    @Singleton
    EndPointInterface providesEndPointInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EndPointInterface.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(EndPointInterface.class);
    }

    @Provides
    @Singleton
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }
}
