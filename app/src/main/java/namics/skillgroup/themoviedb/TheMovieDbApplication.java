package namics.skillgroup.themoviedb;

import android.app.Application;

/**
 * Created by pablo on 24/04/17.
 */

public class TheMovieDbApplication extends Application {

    private static TheMovieDbApplication app;

    private BasicComponent basicComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        basicComponent = DaggerBasicComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();

    }

    public static TheMovieDbApplication app() {
        return app;
    }

    public BasicComponent basicComponent() {
        return basicComponent;
    }

}
