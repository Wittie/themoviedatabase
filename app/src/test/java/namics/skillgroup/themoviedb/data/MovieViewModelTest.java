package namics.skillgroup.themoviedb.data;

import android.databinding.Observable;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by pablo on 25/04/17.
 */
public class MovieViewModelTest {

    static final String MOVIE_TITLE = "MOVIE_TITLE";
    static final String MOVIE_OVERVIEW = "MOVIE_OVERVIEW";
    static final String MOVIE_POSTER_PATH = "MOVIE_POSTER_PATH";
    static final String MOVIE_BACKDROP_PATH = "MOVIE_BACKDROP_PATH";

    MovieViewModel movieViewModel;

    Observable.OnPropertyChangedCallback onPropertyChangedCallback;

    @Before
    public void setUp() {
        movieViewModel = new MovieViewModel();

        onPropertyChangedCallback = mock(Observable.OnPropertyChangedCallback.class);
        movieViewModel.addOnPropertyChangedCallback(onPropertyChangedCallback);
    }

    protected final void verifyChanged() {
        verify(onPropertyChangedCallback).onPropertyChanged(any(Observable.class), eq(0));
    }

    @Test
    public void setResult_shouldSetResultProperlyForAllUsedFields() {
        Result result = mock(Result.class);
        when(result.getTitle()).thenReturn(MOVIE_TITLE);
        when(result.getOverview()).thenReturn(MOVIE_OVERVIEW);
        when(result.getPosterPath()).thenReturn(MOVIE_POSTER_PATH);
        when(result.getBackdropPath()).thenReturn(MOVIE_BACKDROP_PATH);
        movieViewModel.setResult(result);

        verifyChanged();
        assertEquals(MOVIE_TITLE, movieViewModel.title());
        assertEquals(MOVIE_OVERVIEW, movieViewModel.overview());
        assertEquals(MOVIE_POSTER_PATH, movieViewModel.getPosterPath());
        assertEquals(MOVIE_BACKDROP_PATH, movieViewModel.getBackdropPath());
    }
}